import { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { Route, Switch } from "react-router-dom";
import { BrowserRouter } from "react-router-dom/cjs/react-router-dom.min";
import "bootstrap/dist/css/bootstrap.min.css";
import AppNavbar from "./components/AppNavbar";
import Courses from "./pages/Courses";
import Error from "./pages/Error";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Register from "./pages/Register";
import { UserProvider } from "./UserContext";

export default function App() {
	// React context is nothing but a global state to the app.
	// Context helps you broadcast the data and changes to the data

	let token = localStorage.getItem("token");

	const [user, setUser] = useState({
		id: null,
		isAdmin: null,
	});

	const unsetUser = () => {
		setUser({
			id: null,
			isAdmin: null,
		});

		localStorage.clear();
	};

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(typeof data._id !== "undefined"){
				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			}
		})
	}, []);

	return (
		<UserProvider value={{ user, setUser, unsetUser }}>
			<BrowserRouter>
				<AppNavbar />
				<Container fluid>
					<Switch>
						<Route exact path="/" component={Home} />
						<Route exact path="/courses" component={Courses} />
						<Route exact path="/register" component={Register} />
						<Route exact path="/login" component={Login} />
						<Route exact path="/logout" component={Logout} />
						<Route exact path="*" component={Error} />
					</Switch>
				</Container>
			</BrowserRouter>
		</UserProvider>
	);
}
