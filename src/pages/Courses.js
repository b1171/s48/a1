import { Fragment, useContext, useEffect, useState } from "react";
import AdminView from "../components/AdminView";
import UserView from "../components/UserView";
import UserContext from "../UserContext";

export default function Courses() {
	const { user } = useContext(UserContext);
	const [courses, setCourses] = useState([]);

	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/api/courses`)
			.then((res) => res.json())
			.then((data) => {
				setCourses(data);
			});
	};

	useEffect(() => {
		fetchData();
	}, []);

	return (
		<Fragment>
			{
				user.isAdmin
				? 
				<AdminView courseData={courses} fetchData={fetchData} /> 
				: 
				<UserView courseData={courses} />
			}
		</Fragment>
	);
}
