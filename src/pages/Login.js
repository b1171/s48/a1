import { useContext, useEffect, useState } from "react";
import { Container, Form, Button } from "react-bootstrap";
import { Redirect } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function Login() {
	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isDisabled, setIsDisabled] = useState(true);

	useEffect(() => {
		if (email !== "" && password !== "") {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email, password]);

	function Login(e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/api/users/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email: email,
				password: password,
			}),
		})
			.then((res) => res.json())
			.then((res) => {
				
				if (res.access != undefined) {
					localStorage.setItem("token", res.access);
					getUserDetails(res.access);

					Swal.fire({
						title: "Login successful",
						icon: "success",
						text: "Welcome to Course Booking",
					});

					setEmail("");
					setPassword("");
				} else {
					Swal.fire({
						title: "Authentication failed",
						icon: "error",
						text: "Check your login details and try again.",
					});
				}
			});

		function getUserDetails(token) {
			fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
				headers: {
					Authorization: `Bearer ${token}`,
				},
			})
				.then((res) => res.json())
				.then((res) => {
					setUser({
						id: res._id,
						isAdmin: res.isAdmin,
					});

					console.log(res)
				});
		}

	}
	
	return (
		(user.id !== null) ?
		<Redirect to="/courses" />
		:
		<Container className="my-5">
			<Form className="border p-3" onSubmit={(e) => Login(e)}>
				<Form.Group className="mb-3" controlId="email">
					<Form.Label>Email address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email"
						value={email}
						onChange={(e) => {
							setEmail(e.target.value);
						}}
					/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Password"
						value={password}
						onChange={(e) => {
							setPassword(e.target.value);
						}}
					/>
				</Form.Group>

				<Button variant="primary" type="submit" disabled={isDisabled}>
					Login
				</Button>
			</Form>
		</Container>
	);
}
