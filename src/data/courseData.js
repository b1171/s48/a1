const courseData = [
    {
        id: "wdc001",
        name: "PHP - Laravel",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor veniam quibusdam placeat itaque dolorum et alias expedita excepturi facere cum exercitationem vitae voluptatibus aspernatur quas, consequatur natus porro nobis eos?",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc001",
        name: "Java - Springboot",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor veniam quibusdam placeat itaque dolorum et alias expedita excepturi facere cum exercitationem vitae voluptatibus aspernatur quas, consequatur natus porro nobis eos?",
        price: 5000,
        onOffer: true
    },
    {
        id: "wdc001",
        name: "Node - Express",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor veniam quibusdam placeat itaque dolorum et alias expedita excepturi facere cum exercitationem vitae voluptatibus aspernatur quas, consequatur natus porro nobis eos?",
        price: 5500,
        onOffer: true
    }
]

export default courseData;