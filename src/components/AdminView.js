import { Fragment, useEffect, useContext, useState } from "react";
import {
	Button,
	Col,
	Container,
	Form,
	Modal,
	Row,
	Table,
} from "react-bootstrap";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function AdminView(props) {
	const { courseData, fetchData } = props;
	const { HOST, user } = useContext(UserContext);

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [courseId, setCourseId] = useState("");
	const [courses, setCourses] = useState([]);

	const [showAdd, setShowAdd] = useState(false);

	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	const addCourse = (e) => {
		e.preventDefault();

		fetch(`${HOST}/api/courses/create-course`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`,
			},
			body: JSON.stringify({
				courseName: name,
				description: description,
				price: price,
			}),
		})
			.then((res) => {
				if (res !== false) {
					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Course successfully added",
					});

					setName("");
					setDescription("");
					setPrice(0);

					closeAdd();

					fetchData();
				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again",
					});

					fetchData();
				}
			});
	};

	const openEdit = (courseId) => {
		fetch(`${HOST}/api/courses/${courseId}/get-course`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		})
			.then((res) => res.json())
			.then((res) => {
				setName(res.courseName);
				setDescription(res.description);
				setPrice(res.price);
			});

		setShowAdd(true);
	};

	const editCourse = (e, courseId) => {
		e.preventDefault();

		fetch(`${HOST}/api/courses/${courseId}/edit`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
			body: JSON.stringify({
				courseName: name,
				description: description,
				price: price,
			}),
		})
			.then((res) => res.json())
			.then((res) => {
				// console.log(data) //object

				if (typeof res !== "undefined") {
					fetchData();

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Course successfully updated.",
					});

					closeAdd();
				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again.",
					});

					fetchData();
				}
			});
	};

	const archiveCourse = (courseId, isActive) => {
		console.log(isActive);
		fetch(`${HOST}/api/courses/${courseId}/archive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
			body: JSON.stringify({
				isActive: !isActive,
			}),
		})
			.then((res) => res.json())
			.then((res) => {
				// console.log(data)

				if (res === true) {
					fetchData();

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Course disabled",
					});
				} else {
					fetchData();

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again.",
					});
				}
			});
	};

	useEffect(() => {
		const courseArr = courseData.map((course) => {
			return (
				<tr>
					<td>{course.courseName}</td>
					<td>{course.description}</td>
					<td>{course.price}</td>
					<td>
						{course.isActive === true ? (
							<span>Available</span>
						) : (
							<span>Unavailable</span>
						)}
					</td>
					<td>
						<Button onClick={() => openEdit(course._id)}>
							Update
						</Button>
						{course.isActive ? (
							<Fragment>
								<Button
									variant="danger"
									onClick={() =>
										archiveCourse(
											course._id,
											course.isActive
										)
									}
								>
									Disable
								</Button>
								<Button variant="secondary">Delete</Button>
							</Fragment>
						) : (
							<Fragment>
								<Button variant="success">Enable</Button>
								<Button variant="secondary">Disable</Button>
							</Fragment>
						)}
					</td>
				</tr>
			);
		});

		setCourses(courseArr);
	}, [courseData]);

	return (
		<Container>
			<h2 className="text-center">Admin Dashboard</h2>
			<Row className="justify-content-center">
				<Col>
					<div className="text-right">
						<Button onClick={openAdd}>Add New Course</Button>
					</div>
				</Col>
			</Row>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>{courses}</tbody>
			</Table>

			<Modal show={showAdd} onHide={closeAdd}>
				<Modal.Header closeButton>
					<Modal.Title>Add Course</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form onSubmit={(e) => addCourse(e)}>
						<Form.Group controlId="courseName">
							<Form.Label>Course Name:</Form.Label>
							<Form.Control
								type="text"
								value={name}
								onChange={(e) => setName(e.target.value)}
							/>
						</Form.Group>
						<Form.Group controlId="courseDescription">
							<Form.Label>Description:</Form.Label>
							<Form.Control
								type="text"
								value={description}
								onChange={(e) => setDescription(e.target.value)}
							/>
						</Form.Group>
						<Form.Group controlId="coursePrice">
							<Form.Label>Price:</Form.Label>
							<Form.Control
								type="number"
								value={price}
								onChange={(e) => setPrice(e.target.value)}
							/>
						</Form.Group>
						<Button variant="primary" type="submit">
							Submit
						</Button>
						<Button
							variant="secondary"
							type="submit"
							onClick={closeAdd}
						>
							Close
						</Button>
					</Form>
				</Modal.Body>
			</Modal>

			{/* <Modal show={showAdd} onHide={closeAdd}>
				<Modal.Header closeButton>
					<Modal.Title>Edit Course</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form onSubmit={(e) => editCourse(e, courseId)}>
						<Form.Group controlId="courseName">
							<Form.Label>Course Name:</Form.Label>
							<Form.Control
								type="text"
								value={name}
								onChange={(e) => setName(e.target.value)}
							/>
						</Form.Group>
						<Form.Group controlId="courseDescription">
							<Form.Label>Description:</Form.Label>
							<Form.Control
								type="text"
								value={description}
								onChange={(e) => setDescription(e.target.value)}
							/>
						</Form.Group>
						<Form.Group controlId="coursePrice">
							<Form.Label>Price:</Form.Label>
							<Form.Control
								type="number"
								value={price}
								onChange={(e) => setPrice(e.target.value)}
							/>
						</Form.Group>
						<Button variant="success" type="submit">
							Submit
						</Button>
						<Button
							variant="secondary"
							type="submit"
							onClick={closeAdd}
						>
							Close
						</Button>
					</Form>
				</Modal.Body>
			</Modal> */}
		</Container>
	);
}
