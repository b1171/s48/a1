import { Fragment } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";

export default function Banner(props) {

	return (
		<Container fluid className="my-5">
			<Row>
				<Col>
					<div className="jumbotron">
						<BannerContent content={props.content} />
					</div>
				</Col>
			</Row>
		</Container>
	);
}

function BannerContent(props) {
	if (props.content === "home") {
		return (
			<Fragment>
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere.</p>
				<Button variant="primary">Enroll Now</Button>
			</Fragment>
		);
	} else if (props.content === "error") {
		return (
			<Fragment>
				<h1>Page Not Found</h1>
				<p>Go back to the <a href="/" className="text-decoration-none">homepage</a>.</p>
			</Fragment>
		);
	}
}
