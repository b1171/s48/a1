import { Fragment, useEffect, useState } from "react";
import CourseCard from "./CourseCard";

export default function UserView({ courseData }) {

    console.log(courseData)
	const [courses, setCourses] = useState([]);

	useEffect(() => {
		const courseArr = courseData.map((course) => {
			if (course.isActive === "true") {
				return <CourseCard courseProp={course} key={course._id} />;
			} else {
				return null;
			}
		});

		setCourses(courseArr);
	}, [courseData]);

	return <Fragment>{courses}</Fragment>;
}
